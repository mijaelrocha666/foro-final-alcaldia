import { Injectable } from '@angular/core';
import { IPublicacion } from '../models/publicacion.interface';

@Injectable({
  providedIn: 'root'
})
export class PublicacionService {
  post:IPublicacion[]=[];
  constructor() {
    this.post=[
      {titulo:'Mijael',fecha:new Date(),descripcion:'Hola'},
      {titulo:'Mijael',fecha:new Date(),descripcion:'Como estan?...'}
    ];
  }

  obtenerPost(){
    return this.post;
  }

  addPost(publicacion:IPublicacion){
    this.post.push(publicacion);
    return false;
  }
}
